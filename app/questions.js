const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Question = require('../models/Question');
const Response = require('../models/Response');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.get('/', (req, res) => {
        Question.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.get('/:id', (req, res) => {
        Question.find({_id: req.params.id})
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user'), upload.single('image')], async (req, res) => {
        const question = req.body;

        if (req.file) {
            question.image = req.file.filename;
        } else {
            question.image = null;
        }

        // question.responses.map(response => {
        //    console.log(response);
        // });

        const q = new Question(question);

        q.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;