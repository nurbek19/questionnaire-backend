const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ResponseSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    // question: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Question',
    //     required: true
    // },
    vote: {
        type: Number,
        default: 0
    }
});

const Response = mongoose.model('Response', ResponseSchema);

module.exports = Response;